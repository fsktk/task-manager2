#pragma once

#include "IProcessUtils.hpp"

class PlatformLinux : public IProcessUtils
{
public:
    ~PlatformLinux();
    std::vector<Process> GetProcesses();
    int GetUid(pid_t pid);
    void KillProcess(int pid);
};