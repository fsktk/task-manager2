#pragma once

#include "IProcessUtils.hpp"

class PlatformPS : public IProcessUtils
{
public:
    ~PlatformPS();
    std::vector<Process> GetProcesses();
    int GetUid(pid_t pid);
    void KillProcess(int pid);
};