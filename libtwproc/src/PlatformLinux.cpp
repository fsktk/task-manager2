#include <cctype>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <array>
#include <filesystem>
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <string>
#include <signal.h>
#include "Process.hpp"
#include "PlatformLinux.hpp"

enum ProcStatusField : int
{
	PID = 0,
	Comm,
	State,
	Ppid,
	Pgrp,
	Session,
	Tty_nr,
	Tpgid,
	Flags,
	Minflt,
	Cminflt,
	Majflt,
	Cmajflt,
	Utime,
	Stime,
	Cutime,
	Cstime,
	Priority,
	Nice,
	Num_threads,
	Starttime,
	Vsize,
	Rss,
	Rsslim,
	Startcode,
	Endcode,
	Startstack,
	Kstkesp,
	Kstkeip,
	Signal,
	Blocked,
	Sigignore,
	Sigcatch,
	Wchan,
	Nswap,
	Cnswap,
	Exit_signal,
	Processor,
	RT_Priority,
	Policy,
	Delayacct_blkio_ticks,
	Guest_time,
	Cguest_time,
	Start_data,
	End_data,
	Start_brk,
	Arg_start,
	Arg_end,
	Env_start,
	Env_end,
	Exit_code
};

PlatformLinux::~PlatformLinux() {}

// Write the content of a file into a string
std::string GetFileContent(const std::string& path)
{
	std::ifstream file(path);
	std::stringstream buffer;
	buffer << file.rdbuf();
	std::string fileContent = buffer.str();

	return fileContent;
}

// Parse the /proc/[PID]/stat file returns a map (Key: stat file entries)
std::array<std::string, 51> StatParser(const std::string& path)
{
	// array->enum, map->array

	std::array<std::string, 51> processInfo;

	std::string fileContent = GetFileContent(path);

	std::string entry = "";
	unsigned int index = 0;

	for (unsigned int i = 0; i < fileContent.size(); i++)
	{
		if (fileContent[i] != ' ')
		{
			entry += fileContent[i];
		}
		else
		{
			if (!entry.empty() && index < 51)
			{
				processInfo[index] = entry;
				index++;
				entry = "";
			}
		}
	}

	return processInfo;
}

bool IsFileAProcess(const char* filename)
{
	int i = 0;
	while (filename[i] != '\0')
	{
		if (!std::isdigit(filename[0]))
		{
			return false;
		}
		++i;
	}
	return true;
}
// create a vector of running processes
std::vector<Process> PlatformLinux::GetProcesses()
{
	// usleep(300000);
	std::vector<Process> processList;
	std::array<std::string, 51> processInfo;
	for (const auto& entry : std::filesystem::directory_iterator("/proc"))
	{
		const char* filename = entry.path().filename().c_str();
		// check whole file name
		if (IsFileAProcess(filename)) //(std::isdigit(filename[0]))
		{
			std::string f_str = entry.path().filename();

			std::string path = "/proc/";
			path += f_str;
			path += "/stat";

			processInfo = StatParser(path);

			struct passwd* p;
			uid_t uid = GetUid(std::atoi(f_str.c_str()));
			p = getpwuid(uid);

			if (p)
			{
				processList.emplace_back(std::atoi(processInfo[ProcStatusField::PID].c_str()), p->pw_name, processInfo[ProcStatusField::Comm]);
			}
		}
	}

	return processList;
}

// kill the given process
void PlatformLinux::KillProcess(int pid)
{
	kill(pid, 9);
}

// Read content of the /proc/[PID]/status and extract the uid
int PlatformLinux::GetUid(pid_t pid)
{
	std::string statusFilePath = "/proc/" + std::to_string(pid) + "/status";
	std::ifstream statusFile(statusFilePath);

	if (!statusFile.is_open())
	{
		std::cerr << "Error: Unable to open " << statusFilePath << std::endl;
		return -1;
	}

	std::string line;
	while (std::getline(statusFile, line))
	{
		if (line.find("Uid:") == 0)
		{
			std::istringstream iss(line);
			std::string token;
			int uid = -1;
			int i = 0;
			while (std::getline(iss, token, '\t'))
			{
				if (i == 1)
				{
					uid = std::stoi(token);
					break;
				}
				i++;
			}
			statusFile.close();
			return uid;
		}
	}

	statusFile.close();
	return -1;
}
