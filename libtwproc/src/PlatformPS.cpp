#include "PlatformPS.hpp"
#include <csignal>
#include <fstream>
#include <iostream>
#include <sstream>

PlatformPS::~PlatformPS() {}

std::vector<Process> PlatformPS::GetProcesses() {
    std::vector<Process> processes;
    std::string command = "ps aux";
    FILE* pipe = popen(command.c_str(), "r");
    if (!pipe) {
        std::cerr << "Error: Failed to execute command" << std::endl;
        return processes;
    }

    char buffer[128];
    std::string output = "";
    while (!feof(pipe)) {
        if (fgets(buffer, sizeof(buffer), pipe) != NULL) {
            output += buffer;
        }
    }

    int rc = pclose(pipe);
    if (rc != 0) {
        std::cerr << "Error: Command failed with return code " << rc << std::endl;
        return processes;
    }

    std::istringstream iss(output);
    std::string line;
    // Skip the first line (header)
    std::getline(iss, line);

    while (std::getline(iss, line)) {
        std::istringstream lineStream(line);
        std::string field;
        Process process;
        int fieldIndex = 0;

        while (std::getline(lineStream, field, ' ')) {
            if (!field.empty()) {
                switch (fieldIndex) {
                case 0:
                    process.m_user = field;
                    break;
                case 1:
                    process.m_id = std::stoi(field);
                    break;
                default:
                    if (fieldIndex >= 10) {
                        process.m_command += field + " ";
                    }
                    break;
                }
                fieldIndex++;
            }
        }
        
        processes.push_back(process);
    }

    return processes;
}

// kill the given process
void PlatformPS::KillProcess(int pid)
{
	kill(pid, 9);
}

// Read content of the /proc/[PID]/status and extract the uid
int PlatformPS::GetUid(pid_t pid)
{
	std::string statusFilePath = "/proc/" + std::to_string(pid) + "/status";
	std::ifstream statusFile(statusFilePath);

	if (!statusFile.is_open())
	{
		std::cerr << "Error: Unable to open " << statusFilePath << std::endl;
		return -1;
	}

	std::string line;
	while (std::getline(statusFile, line))
	{
		if (line.find("Uid:") == 0)
		{
			std::istringstream iss(line);
			std::string token;
			int uid = -1;
			int i = 0;
			while (std::getline(iss, token, '\t'))
			{
				if (i == 1)
				{
					uid = std::stoi(token);
					break;
				}
				i++;
			}
			statusFile.close();
			return uid;
		}
	}

	statusFile.close();
	return -1;
}