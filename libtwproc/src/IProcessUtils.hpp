#pragma once

#include <vector>
#include <array>
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include "Process.hpp"

class IProcessUtils
{

public:
virtual ~IProcessUtils() {}
virtual std::vector<Process> GetProcesses() = 0;
virtual int GetUid(pid_t pid) = 0;
virtual void KillProcess(int pid) = 0;

// Write the content of a file into a string
// std::string GetFileContent(const std::string& path);

// Parse the /proc/[PID]/stat file returns a map (Key: stat file entries)
// std::array<std::string, 51> StatParser(const std::string& path);

// bool IsFileAProcess(const char* filename);

// create a vector of running processes


}; // namespace ProcessUtils
