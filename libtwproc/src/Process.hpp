#pragma once

#include <string>


class Process
{
public:
	int m_id = 0;
	std::string m_user = "";
	std::string m_command = "";

public:
	Process();

	Process(int id, std::string user, std::string command);

	std::string toString();
};
