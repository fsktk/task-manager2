#include "Process.hpp"


Process::Process() {}

Process::Process(int id, std::string user, std::string command)
	: m_id(id)
	, m_user(user)
	, m_command(command)
{
}

std::string Process::toString()
{
#if 1
	return 
		std::to_string(m_id) + " " + 
		m_user + "\t\t" + 
		// std::to_string(m_cpuUsage) + "\t" + 
		// std::to_string(m_memUsage) +
		m_command + "\t";
#else
	return 
		"PID: " + std::to_string(m_id) + "\t" + 
		"User: " + m_user + "\t" + 
		"Command: " + m_command + "\t" + 
		"CPU: " + std::to_string(m_cpuUsage) + "\t" + 
		"Mem: " + std::to_string(m_memUsage);
#endif
}
