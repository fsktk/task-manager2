#pragma once

#include <GLFW/glfw3.h>
#include <utility>

class GUI {
public:
  int Init(int width, int height, const char *title);
  std::pair<int, int> GetWindowSize();
  void Close();
  GLFWwindow *GetWindow();
  void BeginDrawing();
  void EndDrawing();
  bool ShouldClose();

private:
  GLFWwindow *m_window;
};