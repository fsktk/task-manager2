#pragma once

#include <vector>
#include <array>
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include "Process.hpp"

class IProcessUtils
{
public:
	virtual ~IProcessUtils() {}
	virtual std::vector<Process> GetProcesses() = 0;
	virtual int GetUid(pid_t pid) = 0;
	virtual void KillProcess(int pid) = 0;
};
