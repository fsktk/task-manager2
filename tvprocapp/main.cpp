#include <memory>
#include "GUI.hpp"
#include "PlatformLinux.hpp"
#include "PlatformPS.hpp"
#include "ProcModel.hpp"
#include "ProcView.hpp"
#include "ProcController.hpp"
#include "AppStyles.hpp"

int main()
{
	GUI gui;
	gui.Init(16 * 50, 9 * 50, "Task Manager");
	SetupLightTheme();

	ProcModel procModel{std::make_unique<PlatformLinux>(PlatformLinux())};
	ProcView procView(procModel, gui);
	ProcController procController(procView, procModel, gui);

	procController.Run(gui);

	gui.Close();
	return 0;
}
