#pragma once

#include <thread>

#include "ProcView.hpp"

class ProcController
{
public:
	ProcController(ProcView& view, ProcModel& model, GUI& gui);
	void Run(GUI& gui);

private:
	ProcView& m_view;
	ProcModel& m_model;
	GUI& m_gui;
	std::thread m_updateThread;
};