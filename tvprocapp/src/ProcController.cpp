#include "ProcController.hpp"
#include <iostream>

ProcController::ProcController(ProcView& view, ProcModel& model, GUI& gui)
	: m_view(view)
	, m_model(model)
	, m_gui(gui)
{
	m_updateThread = std::thread(
		[this]()
		{
			while (!m_gui.ShouldClose())
			{
				m_model.Update();
				std::cout << "update thread\n";
				std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			}
		});
}
void ProcController::Run(GUI& gui)
{
	// Main loop
	while (!m_gui.ShouldClose())
	{
		// Poll UI Actions
		if (m_view.KillRequested().first)
		{
			m_model.Kill(m_view.KillRequested().second);
		}
		gui.BeginDrawing();
		{
			m_view.Draw();
		}
		gui.EndDrawing();
	}

	m_updateThread.join();
}