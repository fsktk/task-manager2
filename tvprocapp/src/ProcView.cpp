#include "ProcView.hpp"
#include "GUI.hpp"
#include <imgui.h>
#include <GLFW/glfw3.h>
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"

ProcView::ProcView(ProcModel& model, GUI& gui)
	: m_model(model)
	, m_gui(gui)
{
}

void ProcView::Draw()
{
	float bottom = 50.0f; // height of the button pane
	float buttonWidth = 200.0f;
	float buttonHeight = 30.0f;
	float paddingRight = 25.0f;
	int w = m_gui.GetWindowSize().first;
	int h = m_gui.GetWindowSize().second;

	// Set window flags to remove frame decorations and make it full screen
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize
		| ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse;
	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::SetNextWindowSize(ImVec2(w, h));
	// Begin: full-screen ImGui window
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	ImGui::Begin("Full Screen Window", NULL, window_flags);
	// Update sizes based on user drag input
	// Begin: Process pane
	ImGui::BeginChild("ProcessPane", ImVec2(0, h - bottom - 20), true);
	ImGui::Text("Processes");

	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	if (ImGui::BeginTable("ProcessTable", 3, ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg))
	{
		// Table header
		ImGui::TableSetupColumn("ID", ImGuiTableColumnFlags_WidthFixed);
		ImGui::TableSetupColumn("User", ImGuiTableColumnFlags_WidthFixed);
		ImGui::TableSetupColumn("Command");
		ImGui::TableHeadersRow();

		for (int i = 0; i < m_model.GetProcesses().size(); i++)
		{
			ImGui::TableNextRow();
			// Check if the process is selected
			// Columns
			// PID
			ImGui::TableSetColumnIndex(0);
			if (ImGui::Selectable(std::to_string(m_model.GetProcesses()[i].m_id).c_str(), m_selectedIndex == i, ImGuiSelectableFlags_SpanAllColumns))
			{
				m_selectedIndex = i;
				m_selectedPID = m_model.GetProcesses()[i].m_id;
			}

			// User
			ImGui::TableSetColumnIndex(1);
			ImGui::Text("%s", m_model.GetProcesses()[i].m_user.c_str());
			// Command
			ImGui::TableSetColumnIndex(2);
			ImGui::Text("%s", m_model.GetProcesses()[i].m_command.c_str());
		}

		ImGui::EndTable();
		ImGui::PopStyleVar();
	}

	ImGui::EndChild();

	// Begin: Button pane
	ImGui::BeginChild("ButtonPane", ImVec2(0, bottom), true);

	bool enableEndProcessButton = m_selectedIndex >= 0 && m_selectedIndex < m_model.GetProcesses().size();

	if (!enableEndProcessButton)
	{
		ImGui::BeginDisabled();
	}

	m_killRequested = false;
	ImGui::SameLine(ImGui::GetWindowWidth() - buttonWidth - paddingRight);
	if (ImGui::Button("End Process", ImVec2(buttonWidth, buttonHeight)))
	{
		m_killRequested = true;
	}

	// End the disabled block
	if (!enableEndProcessButton)
	{
		ImGui::EndDisabled();
	}

	ImGui::EndChild();
	// end

	// End full-screen ImGui window
	ImGui::End();
	ImGui::PopStyleVar();
}

std::pair<bool, std::size_t> ProcView::KillRequested()
{
	return std::make_pair(m_killRequested, m_selectedPID);
}
