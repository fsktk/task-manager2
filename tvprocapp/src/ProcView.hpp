#pragma once

#include "GUI.hpp"
#include "ProcModel.hpp"

class ProcView
{
public:
	ProcView(ProcModel& model, GUI& gui);
	void Draw();
	std::pair<bool, std::size_t> KillRequested();

private:
	ProcModel& m_model;
	GUI& m_gui;
	std::size_t m_selectedIndex;
	int m_selectedPID;
	bool m_killRequested = false;
};
