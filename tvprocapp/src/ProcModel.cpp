#include "ProcModel.hpp"

ProcModel::ProcModel(std::unique_ptr<IProcessUtils> platform)
	: m_platform(std::move(platform))
{
	m_procs = m_platform->GetProcesses();
}

std::vector<Process>& ProcModel::GetProcesses()
{
	return m_procs;
}

void ProcModel::Update()
{
	m_procs = m_platform->GetProcesses();
}

void ProcModel::Kill(int pid)
{
	m_platform->KillProcess(pid);
}
