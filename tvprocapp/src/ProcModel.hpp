#pragma once

#include <memory>
#include <vector>
#include "IProcessUtils.hpp"
#include "Process.hpp"

class ProcModel
{
public:
	ProcModel(std::unique_ptr<IProcessUtils> platform);
	std::vector<Process>& GetProcesses();
	void Update();
	void Kill(int pid);

private:
	std::unique_ptr<IProcessUtils> m_platform;
	std::vector<Process> m_procs;
};
