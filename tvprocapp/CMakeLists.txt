project(task_manager)

include(CTest)
enable_testing()

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(THREADS_PREFER_PTHREAD_FLAG ON)


#set(OpenGL_GL_PREFERENCE GLVND)
#find_package(OpenGL REQUIRED)
find_package(PkgConfig REQUIRED)
find_package(Threads REQUIRED)
find_package(glfw3 3.3 REQUIRED)
#find_package(OpenGL)



pkg_search_module(GLFW REQUIRED glfw3)

include_directories(src)
include_directories(external/tvproc)
include_directories(external/tvgui)
set(imgui_srcs
	imgui/imgui.cpp
	imgui/imgui_draw.cpp
	imgui/imgui_demo.cpp
	imgui/imgui_tables.cpp
	imgui/imgui_widgets.cpp

	imgui/backends/imgui_impl_glfw.cpp
	imgui/backends/imgui_impl_opengl3.cpp

	imgui/misc/cpp/imgui_stdlib.cpp
)

set(CMAKE_CXX_FLAGS_DEBUG_INIT "-Wall")
set(CMAKE_CXX_FLAGS_RELEASE_INIT "-Wall")

file(GLOB_RECURSE SOURCE_FILES src/*.cpp)

add_executable(task_manager
	main.cpp
	${imgui_srcs}
	${SOURCE_FILES}
)

target_include_directories(task_manager PRIVATE imgui)
target_include_directories(task_manager PUBLIC ${GLFW_INCLUDE_DIRS})
target_link_libraries(task_manager PRIVATE ${OPENGL_LIBRARY} ${GLFW_STATIC_LIBRARIES} Threads::Threads)

# link_directories("external/")
# target_link_libraries(task_manager twproc)

add_library(twproc STATIC IMPORTED) # or STATIC instead of SHARED
set_target_properties(twproc PROPERTIES
  IMPORTED_LOCATION "${CMAKE_SOURCE_DIR}/external/tvproc/libtwproc.a"
  INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_SOURCE_DIR}/external/tvproc"
)
target_link_libraries(task_manager twproc)

add_library(tvgui STATIC IMPORTED) # or STATIC instead of SHARED
set_target_properties(tvgui PROPERTIES
  IMPORTED_LOCATION "${CMAKE_SOURCE_DIR}/external/tvgui/libtvgui.a"
  INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_SOURCE_DIR}/external/tvgui"
)
target_link_libraries(task_manager tvgui)
target_link_libraries(task_manager glfw)
target_link_libraries(task_manager PRIVATE GL)


set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)